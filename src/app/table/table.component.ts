import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  Output,
  EventEmitter,
  OnInit
} from '@angular/core';
import { Config } from './config';
import { DataTable } from './data';
import { PageRequest } from './pageRequest';
import { HttpClient } from '@angular/common/http';
import { Session } from 'protractor';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class TableComponent {
  constructor(private http:HttpClient){

  }
  @Input()
  public config: Config;

  @Input()
  public data: DataTable<any>;

  public size = 10;
  public pageNumber = 0;

  @Output()
  public newPage: EventEmitter<PageRequest> = new EventEmitter<PageRequest>();

  @Output()
  public selection: EventEmitter<number> = new EventEmitter<number>();

  public changePage(pageNum: number) {
    const num = (pageNum < 0) ? 0 :
      (pageNum >= this.data.lastPage) ? (this.data.lastPage - 1) : pageNum;

    this.pageNumber = num;

    this.newPage.emit({
      page: num,
      size: Number(this.size)
    });
  }

  public onSelect (index: number) {
    this.selection.emit(index + (this.pageNumber * this.size));
  }

  public submit(id,status){
    const body = { id,status }
     this.http.post<any>('/api/submit', body).subscribe(data => {
      alert(data)
})
  }
}
