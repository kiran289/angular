export interface Person {
    name: 'string';
    email: 'string';
    phone: 'string';
    company:'string';
    date_entry:'string';
    org_num:'string';
    address_1:'string';
    city:'string';
    zip:'string';
    geo:'string';
    pan:'string';
    pin:'string';
    id:'string';
    status:'string';
    fee:'string';
    guid:'string';
    date_exit:'string';
    date_first:'string';
    date_recent:'string';
    url:'string';
  }