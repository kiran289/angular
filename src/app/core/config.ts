export const configColumns = [
    {
    header: 'ID',
    value: 'id'
  },{
    header: 'Name',
    value: 'name'
  }, {
    header: 'E-mail',
    value: 'email'
  }, {
    header: 'Phone',
    value: 'phone'
  },
  {
    header: 'Address',
    value: 'address_1'
  },
  {
    header: 'City',
    value: 'city'
  },
  {
    header: 'Company',
    value: 'company'
  },
  {
    header: 'Date Of Entry',
    value: 'date_entry'
  },
  {
    header: 'Date Of Exit',
    value: 'date_exit'
  },
  {
    header: 'First Day',
    value: 'date_first'
  },
  {
    header: 'Recent Date',
    value: 'date_recent'
  },
  {
    header: 'Fee',
    value: 'fee'
  },
  {
    header: 'Geo',
    value: 'geo'
  },
  {
    header: 'Guid',
    value: 'guid'
  },
  {
    header: 'Organisation Number',
    value: 'org_num'
  },
  {
    header: 'Pan',
    value: 'pan'
  },
  {
    header: 'Pin Code',
    value: 'pin'
  },
  {
    header: 'Status',
    value: 'status'
  },
  {
    header: 'URL',
    value: 'url'
  },
  {
    header: 'Zip Code',
    value: 'zip'
  }
];  